import React, { useEffect, useContext, useState, useRef, lazy, Suspense } from 'react'
import Footer from './components/Footer'
import NavBar from './components/NavBar'
import { LocaleContext } from './contexts/Locale'
import { Modal, Header, Button, Icon, Dimmer, Loader } from 'semantic-ui-react'
// import Navigator from './navigator'
import api from './api'
import { UserContext } from './contexts/User'
import BottomNavigation from './components/BottomNavigation'
import { HomeContext } from './contexts/home-context'
import { ErrContext } from './contexts/Err'
import Translate, { translate } from './locales/Translate'
import { setLocale } from 'yup'
import _ from 'lodash'
const Navigator = lazy(() => import('./navigator'))

const renderLoader = () => (
  <div class='wrap'>
    <div class='centerpage loading'>
      <div class='pills'></div>
      <div class='paco'>
        <div class='head'></div>
      </div>
      <div class='text-loader'>
        <p>We are loading some fun contents for you to views!</p>
      </div>
    </div>
  </div>
)

const App = () => {
  const intervalRef = useRef(null)
  const ctx = useContext(UserContext)
  const [appState, setAppState] = useState({
    user: api.getCurrentUser(),
    preferredLocale: localStorage.getItem('preferredLocale') || 'id',
    logout: () => {
      api.setCurrentUser(null)
    },
    showErr: false,
    ErrMessage: false,
    ErrgoToPage: false,
    ErrType: 'Notification',
    setErr: (show, message, redirect, type) => {
      setAppState(appState => ({
        ...appState,
        showErr: show,
        ErrType: type,
        ErrMessage: message,
        ErrgoToPage: redirect,
      }))
    },
    gameLoader: false,
    setGameLoader: () => {
      setLoader()
    },
    // sitesInfo: api.getSitesInfo(),
    sitesInfo: false,
    changeLanguage: newLanguage => {
      localStorage.setItem('preferredLocale', newLanguage)
      setAppState(appState => ({ ...appState, preferredLocale: newLanguage }))
      // window.location.reload()
    },
    isMaintenanceMode: null,
  })

  const setLoader = () => {
    setAppState(prevState => ({
      ...prevState,
      gameLoader: !prevState.gameLoader,
    }))
  }

  const getSitesInfo = async () => {
    const sites = await api
      .getBannersList()
      .then(x => {
        api.setSitesInfo(x)
        return x
      })
      .catch(e => {
        console.log('sites errpor', e)
        throw e
      })
    if (!ctx.user) {
      setAppState(appState => ({ ...appState, sitesInfo: sites, isMaintenanceMode: sites.isMaintenanceMode }))
    }
    return sites
  }

  const fetchAndUpdate = () => {
    if (appState.user === null) return false

    Promise.all([getSitesInfo(), api.getBalance(ctx.user.userName)]).then(resolve => {
      const sites = resolve[0]
      const balance = resolve[1]

      console.log({
        sites,
        balance,
      })
      if (balance.Result === 'SUSPEND') {
        clearInterval(intervalRef.current)
        intervalRef.current = null
        appState.setErr(true, `Your account has been ${balance.Result}, please contact support`, 'account_suspend')
        return
      }
      if (sites.isMaintenanceMode) {
        clearInterval(intervalRef.current)
        intervalRef.current = null
        appState.logout()
        window.location = '/'
        return
      }
      const newUser = {
        ...ctx.user,
        ...balance,
        lastUpdated: api.getTimestampFormatted(),
      }
      api.setCurrentUser(newUser)
      setAppState(appState => ({
        ...appState,
        user: newUser,
        sitesInfo: sites,
        isMaintenanceMode: sites.isMaintenanceMode,
      }))
    })
  }

  useEffect(() => {
    if (!localStorage.getItem('preferredLocale')) {
      localStorage.setItem('preferredLocale', 'id')
    }

    getSitesInfo()
    fetchAndUpdate()

    if (appState.user) {
      intervalRef.current = setInterval(() => {
        fetchAndUpdate()
      }, 3000)
    }

    window._slaaskSettings = {
      identify: function() {
        if (appState.user)
          return {
            id: `${appState.user.userName}`, // User ID (Mandatory)
            name: `${appState.user.userName}`, // Your user's full name
            email: `${appState.user.userName}`, // Your user's email address
          }
        else return null
      },

      key: 'spk-e2149524-eee9-45f8-837b-b5935df781d2',
    }

    return () => {
      clearInterval(intervalRef.current)
    }
  }, [])

  //   const setContext = useCallback(update => {
  //     setAppState({ ...appState, ...update })
  //   })

  //   const getContextValue = useCallback(
  //     () => ({
  //       ...appState,
  //       setContext,
  //     }),
  //     [appState, setContext],
  //   )

  const handleCloseShowErr = () => {
    setAppState({
      ...appState,
      showErr: false,
    })
    if (appState.ErrgoToPage === 'account_suspend') {
      appState.logout()
      window.location = '/'
    }
  }

  return (
    <div>
      <LocaleContext.Provider value={appState}>
        <UserContext.Provider value={appState}>
          <HomeContext.Provider value={appState}>
            <ErrContext.Provider value={appState}>
              <div className={'stickyNavbar'}>
                <NavBar />
              </div>

              {/* {renderLoader()} */}
              <Suspense fallback={renderLoader()}>
                <Navigator style={{ flex: 1, minHeight: '100vh' }} />
              </Suspense>
              <Dimmer.Dimmable blurring dimmed={appState.gameLoader}>
                <Dimmer active={appState.gameLoader} inverted page>
                  <Loader active={appState.gameLoader} inline='centered' size='massive'>
                    <Translate string='gameLoading' />
                    <br />
                    <br />

                    <Button
                      icon='refresh'
                      size='big'
                      color='red'
                      content='Refresh'
                      labelPosition='left'
                      onClick={e => {
                        e.preventDefault()
                        window.location.reload()
                      }}
                    />
                  </Loader>
                </Dimmer>
              </Dimmer.Dimmable>
              <Footer />
              {/* Only showing on mobile view */}
              <BottomNavigation />
            </ErrContext.Provider>
          </HomeContext.Provider>
        </UserContext.Provider>
      </LocaleContext.Provider>
      <Modal
        open={appState.showErr}
        onClose={handleCloseShowErr}
        header='Reminder!'
        size='tiny'
        closeOnEscape={false}
        closeOnDimmerClick={false}
        dimmer='blurring'>
        {appState.ErrType !== 'Error' ? (
          <Header icon='bullhorn' content='Notification / การแจ้งเตือน' />
        ) : (
          <Header icon='ban' color='red' content='Ooops..Something just happened!' />
        )}

        <Modal.Content>
          <Modal.Description>
            <div style={{ color: 'black' }}>
              <h3>{appState.ErrMessage}</h3>
            </div>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button color='green' onClick={handleCloseShowErr} inverted>
            <Icon name='checkmark' /> Got it
          </Button>
        </Modal.Actions>
      </Modal>

      <Dimmer active={appState.isMaintenanceMode} page>
        <div>{process.env.REACT_APP_MAIN_PARENT} Under Maintenance</div>
      </Dimmer>
    </div>
  )
}

App.gameLoader = ctx => {
  //   console.log(ctx)
  ctx.setGameLoader()
}

App.showErr = (ctx, message) => {
  //   console.log(ctx)
  ctx.setErr(true, translate(ctx.preferredLocale, message))
}

export default App
