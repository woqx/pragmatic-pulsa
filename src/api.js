import * as CryptoJS from 'crypto-js'
import * as _ from 'lodash'
import moment from 'moment-timezone'
import ApolloClient, { gql } from 'apollo-boost'
import { createApolloFetch } from 'apollo-fetch'
import jwt from 'jwt-decode'
import crypto from 'crypto'

class api {
  static backendUrl = process.env.REACT_APP_BACKEND_URL
  static walletUrl = process.env.REACT_APP_WALLET_URL

  static hostName = 'https://' + window.location.hostname
  static paymentUrl = process.env.REACT_APP_BACKEND_URL + 'paytrust/deposit/start'
  static Graph_uri = process.env.REACT_APP_GRAPHQL_URL
  static isAppMobile = /Mobi/.test(navigator.userAgent)
  static agentName = process.env.REACT_APP_MAIN_PARENT
  static milhouseURL = process.env.REACT_APP_MILHOUSE_URL
  static hababrandid = process.env.REACT_APP_HABANERO_BRANDID
  static sID = process.env.REACT_APP_TWILIO_SID

  static elyLaunchURL = process.env.REACT_APP_ELY_LAUNCH_URL || 'https://elysium.texas888.net'

  static client = new ApolloClient({
    uri: process.env.REACT_APP_GRAPHQL_URL,
  })

  static queryPlayersBet = `
    query PBS($playerName: String!, $dateStart: String!, $dateEnd: String!, $transactionStartDate: String!) {
      pbs(playerName: $playerName, dateStart: $dateStart, dateEnd: $dateEnd) {
        date
        validTurnover
        playerWinLoss
      }
      
      playerTransactions(
        first: 10
        dateStart: $transactionStartDate
        dateEnd: $dateEnd
        playerName: $playerName
        providerName: "ADMIN"
      ) {
        offset
        count
        playerLines {
          playerName
          transactionTime
          type
          winLose
          status
          providerName
        }
      }
      
    }
    
  `

  static PBDQuery = gql`
    query(
      $first: Int!
      $offset: Int!
      $playerName: String!
      $dateStart: String!
      $dateEnd: String!
      $agentName: String!
    ) {
      playerBetDetails(
        first: $first
        offset: $offset
        providerName: ""
        agentName: $agentName
        playerName: $playerName
        dateStart: $dateStart
        dateEnd: $dateEnd
      ) {
        offset
        count
        playerLines {
          playerName
          transactionTime
          transaction
          api_timestamp
          type
          stake
          winLose
          status
          providerName
          afterBalance
          freeGame
          bets
        }
      }
    }
  `

  static getCurrentUser() {
    // console.log(process.env.REACT_APP_BACKEND_URL)
    var res = sessionStorage.getItem('cronos')
    if (_.isEmpty(res)) {
      return null
    } else {
      return JSON.parse(atob(res))
    }
  }

  static setCurrentUser(userJson) {
    const x = btoa(JSON.stringify(userJson))
    return sessionStorage.setItem('cronos', x)
  }

  static getSitesInfo() {
    return JSON.parse(sessionStorage.getItem('dv-sitesInfo'))
  }

  static setSitesInfo(sites) {
    return sessionStorage.setItem('dv-sitesInfo', JSON.stringify(sites))
  }

  static logout() {
    const user = this.getCurrentUser()
    window._slaask.destroy()
    return Promise.all([api.kickIon(user), api.kickJDB(user)]).then(([x, y]) => {
      sessionStorage.clear()
    })
  }

  static getPaytrustUrl(userName, amount) {
    return fetch(
      `${this.paymentUrl}?amount=${amount}&name=${userName}&returnUrl=${this.hostName}/thank-you&failedUrl=${this.hostName}/profile`,
    )
  }

  static getCheckSum(str) {
    return btoa(CryptoJS.MD5.hashStr(str, true).toString())
  }

  static getTimestampFormatted() {
    const datenow = moment(new Date())
    const re = datenow.tz('America/Bogota').format()
    return re.toString().slice(0, -6)
  }

  static checkIfUsernameExist(playerName) {
    const uri = this.Graph_uri
    const query = `query attrByUsername ($playerName: String!) {
                checkuserByAttr(
                  playerName: $playerName
                )
                {
                  exist
                }
              }`

    const variables = {
      playerName: playerName,
    }
    const apolloFetch = createApolloFetch({ uri })
    return apolloFetch({ query, variables })
      .then(x => x)
      .catch(e => console.error(e))
    //return fetch(this.backendUrl + 'user/checkUser?userName=' + username.toLowerCase()).then((x) => x.json()).catch((e) => console.error(e));
  }

  static checkIfAgentNameExist(agentName) {
    return fetch(this.backendUrl + 'checkAgent?agentName=' + agentName.toLowerCase())
      .then(x => x.json())
      .catch(e => console.error(e))
  }

  static checkIfEMailExist(eMail) {
    // return fetch(this.backendUrl + 'users/search?email=' + eMail.toLowerCase())
    //   .then(x => x.json())
    //   .catch(e => console.error(e))
    const uri = this.Graph_uri
    const query = `query attrByUsername ($email: String!) {
                checkuserByAttr(
                  email: $email
                )
                {
                  exist
                }
              }`

    const variables = {
      email: eMail,
    }
    const apolloFetch = createApolloFetch({ uri })
    return apolloFetch({ query, variables })
      .then(x => x)
      .catch(e => console.error(e))
  }

  static getParentInfo(username) {
    return fetch(`${this.milhouseURL}/user/${username}`, {
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
      },
    })
  }

  static getChildrenList(username) {
    return fetch(`${this.milhouseURL}/users/parent/${username}`, {
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
      },
    })
  }

  static withdrawChildrenAmount(username, amount) {
    return fetch(`${this.milhouseURL}/user/${username}/withdraw`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.getCurrentUser().token,
      },
      body: JSON.stringify({
        amount,
      }),
    })
  }

  static checkIfTelephoneExist(telephone) {
    const uri = this.Graph_uri
    const query = `query attrByTelephone ($agentName: String!, $telephoneNo: String!) {
                checkuserByAttr(
                  agentName: $agentName,
                  telephoneNo: $telephoneNo
                )
                {
                  exist
                }
              }`

    const variables = {
      agentName: this.agentName,
      telephoneNo: telephone,
    }
    const apolloFetch = createApolloFetch({ uri })
    return apolloFetch({ query, variables })
      .then(x => x)
      .catch(e => console.error(e))
    //return fetch(this.backendUrl + 'users/search?telephone=' + encodeURIComponent(telephone)).then((x) => x.json()).catch((e) => console.error(e))
  }

  static checkifBankAccountExist(bankAccount) {
    const uri = this.Graph_uri
    const query = `query attrByBankAccount ($agentName: String!, $accountNo: String!) {
                checkuserByAttr(
                  agentName: $agentName,
                  accountNo: $accountNo
                )
                {
                  exist
                }
              }`

    const variables = {
      agentName: this.agentName,
      accountNo: bankAccount,
    }
    const apolloFetch = createApolloFetch({ uri })
    return apolloFetch({ query, variables })
      .then(x => x)
      .catch(e => console.error(e))

    //return fetch(this.backendUrl + 'users/search?bankaccount=' + bankAccount).then((x) => x.json()).catch((e) => console.error(e))
  }

  static registerUser(userJson) {
    const users = JSON.parse(userJson)
    const hash = crypto
      .createHash('md5')
      .update(`${users.userName}${users.password}${this.agentName}REGIS`)
      .digest('hex')
    return fetch(this.backendUrl + 'user/v2/insert', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-Sig': hash,
      },
      body: userJson,
    })
  }

  static changePassword(password) {
    return fetch(this.backendUrl + 'user/pwd', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.getCurrentUser().token,
      },
      body: JSON.stringify({
        password,
      }),
    })
  }

  static login(loginCredentials) {
    return fetch(this.backendUrl + 'user/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: loginCredentials,
    })
      .then(x => x.json())
      .catch(e => console.error(e))
  }

  static getBalance() {
    return fetch(`${this.backendUrl}user/balance?r=${Math.random()}`, {
      headers: {
        'cache-control': 'no-cache',
        Authorization: 'Bearer ' + (this.getCurrentUser() ? this.getCurrentUser().token : null),
      },
    })
      .then(x => x.json())
      .catch(e => console.error(e))
  }

  static toBase64 = file =>
    new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.readAsDataURL(file)
      reader.onload = () => resolve(reader.result)
      reader.onerror = error => reject(error)
    })

  static async depositMoney(username, amount, transferBy, file) {
    let image = false
    if (file) {
      image = await this.toBase64(file)
    }

    const hash = crypto
      .createHash('md5')
      .update(`${username}${amount}${this.agentName}`)
      .digest('hex')
    return fetch(
      this.walletUrl + `admin/user/deposit?r=${Math.random()}`,
      // `user/deposit?username=${username.toLowerCase()}&amount=${amount}&actionBy=${username.toLowerCase()}`,
      {
        headers: {
          Authorization: 'Bearer ' + this.getCurrentUser().token,
          'X-Sig': hash,
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          amount,
          actionBy: username.toLowerCase(),
          pulsa: transferBy === 'pulsatsel' || transferBy === 'pulsaxl' ? true : false,
          ovoGoPay: transferBy === 'ovoGoPay' ? true : false,
          receipt: image,
        }),
        method: 'POST',
      },
    )
  }

  static withdrawMoney(username, amount) {
    return fetch(this.walletUrl + `admin/user/withdraw?r=${Math.random()}`, {
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        amount,
      }),
      method: 'POST',
    })
  }

  static getBankAccount() {
    return fetch(this.backendUrl + 'user/bank', {
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
      },
    })
  }

  static getBankList() {
    return fetch(`${this.backendUrl}getBanksIDR/${this.agentName}?r=${Math.random()}`, {
      method: 'POST',
    })
      .then(x => x.json())
      .catch(e => console.error(e))
  }

  static getUser(username) {
    return fetch(this.backendUrl + 'user/get?username=' + username.toLowerCase(), {
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
      },
    })
  }

  static kickJDB() {
    const dateNow = new Date()
    const json = {
      action: 17,
      ts: dateNow.getTime(),
      parent: 'cadyag',
    }
    return fetch(this.backendUrl + 'JDB', {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ x: json }),
    })
      .then(x => x.json())
      .catch(e => console.error(e))
  }

  static kickIon() {
    return fetch(`${this.backendUrl}casino/LogOutPlayer`, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(x => x.json())
      .catch(e => console.error(e))
    //return Promise.resolve()
  }

  static kickSpade() {
    return fetch(`${this.backendUrl}spade/kick`, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(x => x.json())
      .catch(e => console.error(e))
    //return Promise.resolve()
  }

  static loginSBO() {
    const lang = localStorage.getItem('preferredLocale') === 'en' ? 'en' : 'th-TH'

    const appType = api.isAppMobile ? 2 : 1

    return fetch(this.backendUrl + 'sbobet/login', {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ lang, device: appType }),
    })
  }

  static getGamesList() {
    //return fetch(`SlotsGoldGamesList.json?noCache=${Math.random()}`)
    return fetch(`${this.backendUrl}getGames/${this.agentName}?r=${Math.random()}`).catch(e => console.error(e))
  }

  static enterDragoon(game_id) {
    // const lang = localStorage.getItem('preferredLocale') === 'en' ? 'en_us' : 'th_th'
    const lang = 'en_us'
    const theBody = {
      game_id,
      lang,
      backurl: `${window.location.href}`,
    }

    return fetch(`${this.backendUrl}dragoon/launch`, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + this.getCurrentUser().token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(theBody),
    })
  }

  static enterMJS() {
    const theBody = {
      platform: this.isAppMobile ? 'mobile' : 'desktop',
    }
    return fetch(`${this.backendUrl}mjs/launch`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.getCurrentUser().token,
      },
      body: JSON.stringify(theBody),
    })
  }

  static enterELY() {
    const user = jwt(this.getCurrentUser().token)
    return `${this.elyLaunchURL}/?e=alpha&token=${user.yggToken}`
  }

  static getBannersList() {
    // return fetch('bannerData.json').then((x) => x.json()).catch((e) => console.error(e))
    return fetch(`${this.backendUrl}getBanners/${this.agentName}?r=${Math.random()}`, {
      method: 'POST',
    })
      .then(x => x.json())
      .catch(e => console.error(e))
  }

  static getPromos() {
    return fetch(`${this.backendUrl}getARESfaqs/${this.agentName}?r=${Math.random()}`, {
      method: 'POST',
    })
      .then(x => x.json())
      .catch(e => console.error(e))
  }

  static getUMStatusSBO() {
    return fetch(this.backendUrl + 'sbobet/status', {
      method: 'POST',
    })
      .then(x => x.json())
      .catch(e => console.error(e))
  }

  static validatePhoneNumber(phoneNumber, channel) {
    const theBody = {
      locale: localStorage.getItem('preferredLocale') || 'th',
      phoneNumber,
      agentName: this.agentName,
      channel,
      sID: this.sID,
    }

    return fetch(`${this.backendUrl}sms/check`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(theBody),
    })
  }

  static verifyOTP(phoneNumber, code) {
    const theBody = {
      phoneNumber,
      code,
      sID: this.sID,
    }
    return fetch(`${this.backendUrl}sms/verify`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(theBody),
    })
  }

  static resetPassword(to) {
    const theBody = {
      to,
      agentName: this.agentName,
      hostName: this.hostName,
    }

    return fetch(`${this.backendUrl}user/resetPassword`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(theBody),
    })
  }
  /* Game Launching */
  static launchGame = (ctx, game, demo) => {
    let headers = false
    if (!demo) {
      headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.getCurrentUser().token,
      }
    } else {
      headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }

    /* JDB */
    if (game.provider === 'JDB') {
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          action: 21,
          mType: game.id,
          gType: game.gType.toString(),
          lobbyURL: `${window.location.href}`,
          parent: process.env.REACT_APP_JDB_PARENT,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }
    /* ION CASINO */
    if (game.provider === 'ION') {
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          lang: `${localStorage.getItem('preferredLocale')}-${localStorage.getItem('preferredLocale').toUpperCase()}`,
          view: this.isAppMobile ? 'MOBILE' : 'TABLET',
          product: game.id,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }
    /* SPADEGAMING */
    if (game.provider === 'SPADE') {
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          gameId: game.id,
          lang: `${localStorage.getItem('preferredLocale')}_${localStorage.getItem('preferredLocale').toUpperCase()}`,
          appType: api.isAppMobile ? 'true' : 'false',
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }
    /* HABANERO */
    if (game.provider === 'haba' || game.provider === 'HABA') {
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          brandid: `${this.hababrandid}`,
          keyname: game.id,
          mode: demo ? 'fun' : 'real',
          lang: localStorage.getItem('preferredLocale'),
          lobbyurl: window.location.href,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }
    /* YGGDRASIL */
    if (game.provider === 'YGG') {
      if (demo) {
        return fetch(`${this.backendUrl}launch-game`, {
          method: 'POST',
          headers,

          body: JSON.stringify({
            provider: game.provider,
            gameid: game.id,
            lang: localStorage.getItem('preferredLocale'),
            channel: this.isAppMobile ? 'mobile' : 'pc',
            home: window.location.href,
            fun: true,
          }),
        })
          .then(json => json.json())
          .then(res => {
            return res
          })
      }
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          gameid: game.id,
          lang: localStorage.getItem('preferredLocale'),
          channel: this.isAppMobile ? 'mobile' : 'pc',
          home: window.location.href,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }
    /* RTG */
    if (game.provider === 'RTG') {
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          gameId: game.id,
          lang: localStorage.getItem('preferredLocale') === 'en' ? 'en-US' : 'th-TH',
          returnURL: `${window.location.href}`,
          isDemo: false,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }
    /* Sexy Baccarat */
    if (game.provider === 'SXY') {
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          appType: api.isAppMobile ? '1' : '0',
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }

    /* GENESIS GROUP */
    if (game.provider === 'GNS' || game.provider === 'R8' || game.provider === 'BFX') {
      if (demo) {
        return fetch(`${this.backendUrl}launch-game`, {
          method: 'POST',
          headers,
          body: JSON.stringify({
            provider: game.provider,
            gameid: game.id,
            returnurl: window.location.href,
            demo: true,
            lang: localStorage.getItem('preferredLocale'),
          }),
        })
          .then(json => json.json())
          .then(res => {
            return res
          })
      }
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          gameid: game.id,
          returnurl: window.location.href,
          lang: localStorage.getItem('preferredLocale'),
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }

    /* PGS */
    if (game.provider === 'PGS') {
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          gameid: game.id,
          bet_type: demo ? '2' : '1',
          returnurl: window.location.href,
          platform: this.isAppMobile ? 'MOBILE' : 'WEB',
          lang: localStorage.getItem('preferredLocale'),
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }

    /* PRAGMA */
    if (game.provider === 'PRAG') {
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          gameid: game.id,
          returnurl: window.location.href,
          platform: this.isAppMobile ? 'MOBILE' : 'WEB',
          lang: localStorage.getItem('preferredLocale'),
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }
    /* MAJAGAMES */
    if (game.provider === 'MJG') {
      let theBody = {
        provider: game.provider,
        game_id: game.id,
        is_demo: demo,
        orientation: 'portrait',
        callback_url: window.location.href,
        platform: this.isAppMobile ? 'mobile' : 'desktop',
      }
      if (!this.isAppMobile) {
        delete theBody.orientation
      }

      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify(theBody),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }
    /* EVOPLAY */
    if (game.provider === 'EVO') {
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          game: game.id,
          lang: localStorage.getItem('preferredLocale') || 'en',
          exit_url: window.location.href,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }
    /* ISOFTBET */
    if (game.provider === 'ISB') {
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          gameId: game.id,
          lang: `${localStorage.getItem('preferredLocale').toUpperCase()}`,
          gameMode: _.isEmpty(this.getCurrentUser()) ? 0 : 1,
          lobbyURL: `${window.location.href}`,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }

    /* QT-Gaming */
    if (game.provider === 'QTG') {
      if (demo) {
        return fetch(`${this.backendUrl}launch-game`, {
          method: 'POST',
          headers,
          body: JSON.stringify({
            provider: game.provider,
            gameId: game.id,
            mode: 'demo',
            device: this.isAppMobile ? 'mobile' : 'desktop',
            returnUrl: window.location.href,
            lang: `${localStorage.getItem('preferredLocale')}_${localStorage.getItem('preferredLocale').toUpperCase()}`,
          }),
        })
          .then(json => json.json())
          .then(res => {
            return res
          })
      }
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          gameId: game.id,
          mode: 'real',
          device: this.isAppMobile ? 'mobile' : 'desktop',
          returnUrl: window.location.href,
          lang: `${localStorage.getItem('preferredLocale')}_${localStorage.getItem('preferredLocale').toUpperCase()}`,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }
    /* REDTIGER */
    // if (game.provider === 'RED') {
    //   if (demo) {
    //     return fetch(`${this.backendUrl}launch-game`, {
    //       method: 'POST',
    //       headers,
    //       body: JSON.stringify({
    //         provider: game.provider,
    //         gameId: game.id,
    //         casino: 'PPULSA', //Remember change this VALUES
    //         returnurl: window.location.href,
    //         demo: true,
    //       }),
    //     })
    //   }
    //   return fetch(`${this.backendUrl}launch-game`, {
    //     method: 'POST',
    //     headers,
    //     body: JSON.stringify({
    //       provider: game.provider,
    //       gameId: game.id,
    //       casino: 'PPULSA',
    //       returnurl: window.location.href,
    //     }),
    //   })
    //     .then(json => json.json())
    //     .then(res => {
    //       return res
    //     })
    // }
    /* DREAMTEACH */
    if (game.provider === 'DT') {
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          GAMECODE: game.id,
          CLOSEURL: window.location.href,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }

    /* ENDORPHINA WIP*/
    if (game.provider === 'SNM') {
      return fetch(`${process.env.REACT_APP_SNM_LAUNCH_URL}/launchGame`, {
        method: 'POST',
        headers: {
          Authorization: 'Bearer ' + this.getCurrentUser().token,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          provider: game.provider,

          exit: `${window.location.href}`,
          game: game.id,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }

    /* BETSOFT */
    if (game.provider === 'BSG') {
      if (demo) {
        return fetch(`${this.backendUrl}launch-game`, {
          method: 'POST',
          headers,

          body: JSON.stringify({
            provider: game.provider,
            gameId: game.id,
            bankid: process.env.REACT_APP_BSG_BANKID,
            lang: `${localStorage.getItem('preferredLocale')}`,
            demo: true,
          }),
        })
          .then(json => json.json())
          .then(res => {
            return res
          })
      } else {
        return fetch(`${this.backendUrl}launch-game`, {
          method: 'POST',
          headers,

          body: JSON.stringify({
            provider: game.provider,
            returnurl: `${window.location.href}`,
            gameId: game.id,
            bankid: process.env.REACT_APP_BSG_BANKID,
            lang: `${localStorage.getItem('preferredLocale')}`,
          }),
        })
          .then(json => json.json())
          .then(res => {
            return res
          })
      }
    }

    /* KINGMAKER */
    if (game.provider === 'KTB') {
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          gameCode: game.id,
          returnurl: `${window.location.href}`,
          isMobile: this.isAppMobile,
          lang: 'en',
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }

    /* GAMATRON */
    if (game.provider === 'GMT') {
      if (demo) {
        return fetch(`${this.backendUrl}launch-game`, {
          method: 'POST',
          headers,
          body: JSON.stringify({
            provider: game.provider,
            game: game.id,
            lobbyURL: `${window.location.href}`,
            demo: true,
            locale: `${localStorage.getItem('preferredLocale')}`,
          }),
        })
          .then(json => json.json())
          .then(res => {
            return res
          })
      }

      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          game: game.id,
          lobbyURL: `${window.location.href}`,
          locale: `${localStorage.getItem('preferredLocale')}`,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }

    /* PlayNGo */
    if (game.provider === 'PNG') {
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          origin: `${window.location.href}`,
          gid: game.id,
          lang: `${localStorage.getItem('preferredLocale')}_${localStorage.getItem('preferredLocale').toUpperCase()}`,
          channel: this.isAppMobile ? 'mobile' : 'desktop',
          practice: demo ? 1 : 0,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }

    /* SIMPLEPLAY */
    if (game.provider === 'ASP') {
      if (demo) {
        return fetch(`${this.backendUrl}launch-game`, {
          method: 'POST',
          headers,
          body: JSON.stringify({
            provider: game.provider,
            GameCode: game.id,
            demo: true,
            Lang: `${localStorage.getItem('preferredLocale')}`,
            Mobile: this.isAppMobile ? 1 : 0,
          }),
        })
          .then(json => json.json())
          .then(res => {
            return res
          })
      }

      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          GameCode: game.id,
          demo: false,
          Lang: `${localStorage.getItem('preferredLocale')}`,
          Mobile: this.isAppMobile ? 1 : 0,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }

    /* DRAGOONSOFT */
    if (game.provider === 'DRG') {
      const lang = localStorage.getItem('preferredLocale') === 'th' ? 'th_th' : 'en_us'
      const theBody = {
        provider: game.provider,
        game_id: game.id,
        lang,
        backurl: `${window.location.href}`,
      }

      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify(theBody),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }

    /* SAGAMING */

    if (game.provider === 'SAG') {
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          lang: `${
            localStorage.getItem('preferredLocale') === 'en' ? 'en_US' : localStorage.getItem('preferredLocale')
          }`,
          mobile: this.isAppMobile ? true : false,
          returnurl: `${window.location.href}`,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }
    /* BBTECH */
    if (game.provider === 'BBT') {
      if (demo) {
        return fetch(`${this.backendUrl}launch-game`, {
          method: 'POST',
          headers,
          body: JSON.stringify({
            provider: game.provider,
            demo: true,
            gameconfig: game.id,
            lobby: window.location.href,
            lang: `${localStorage.getItem('preferredLocale')}`,
          }),
        })
          .then(json => json.json())
          .then(res => {
            return res
          })
      }

      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          gameconfig: game.id,
          lobby: `${window.location.href}`,
          lang: `${localStorage.getItem('preferredLocale')}`,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }

    /* PLAYSTAR */
    if (game.provider === 'IPS') {
      if (demo) {
        return fetch(`${this.backendUrl}launch-game`, {
          method: 'POST',
          headers,
          body: JSON.stringify({
            provider: game.provider,
            demo: true,
            game_id: game.id,
            return_url: window.location.href,
            lang:
              localStorage.getItem('preferredLocale') === 'en'
                ? 'en-US'
                : `${localStorage.getItem('preferredLocale')}-${localStorage.getItem('preferredLocale').toUpperCase()}`,
          }),
        })
          .then(json => json.json())
          .then(res => {
            return res
          })
      }

      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          game_id: game.id,
          return_url: `${window.location.href}`,
          lang:
            localStorage.getItem('preferredLocale') === 'en'
              ? 'en-US'
              : `${localStorage.getItem('preferredLocale')}-${localStorage.getItem('preferredLocale').toUpperCase()}`,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }

        /* MANNAPLAY */
    if (game.provider === 'MNP') {
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          gameId: game.id,
          lang: localStorage.getItem('preferredLocale'),
          exitUrl: `${window.location.href}`,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }
    /* ASPECTGAMING */
    if (game.provider === 'APG') {
      if (demo) {
        return fetch(`${this.backendUrl}launch-game`, {
          method: 'POST',
          headers,
          body: JSON.stringify({
            provider: game.provider,
            demo: true,
            gameId: game.id,
          }),
        })
          .then(json => json.json())
          .then(res => {
            return res
          })
      }
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          gameId: game.id,
          operatorId: 1100,
          lang:
            localStorage.getItem('preferredLocale') === 'en'
              ? 'en-US'
              : `${localStorage.getItem('preferredLocale')}-${localStorage.getItem('preferredLocale').toUpperCase()}`,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }

    /* AMEBA ENTERTAINMENT*/
    if (game.provider === 'AMS') {
      return fetch(`${this.backendUrl}launch-game`, {
        method: 'POST',
        headers,
        body: JSON.stringify({
          provider: game.provider,
          gameId: game.id,
          lang:
            localStorage.getItem('preferredLocale') === 'en'
              ? 'enUS'
              : `${localStorage.getItem('preferredLocale')}${localStorage.getItem('preferredLocale').toUpperCase()}`,
          exit_url: `${window.location.href}`,
        }),
      })
        .then(json => json.json())
        .then(res => {
          return res
        })
    }
    /* IF NO PROVIDER return this */
    ctx.setErr(true, 'Game Not Available', '')
    ctx.setGameLoader()
    throw Error
  }
}

export default api
