import React, { Component } from 'react'
import { Icon } from 'semantic-ui-react'

import axios from 'axios'

class FavGame extends Component {
  constructor(props) {
    super(props)
    this.state = {
      game: this.props.game,
      user: this.props.user,
    }
  }

  setFav = async newList => {
    this.props.favLoader('Add')
    newList.mType = newList.id
    newList.isShow = true
    newList.image = newList.image.substring(newList.image.indexOf('/thumbnail'))
    delete newList.categoryIds
    delete newList.id
    delete newList.gType
    delete newList.poster
    delete newList.isNew
    delete newList.isRecommend
    axios
      .post(`${process.env.REACT_APP_FAVORITE_LIST}/updateFavList`, null, {
        params: {
          user: this.state.user,
          list: newList,
        },
      })
      .then(res => {
        this.props.favLoader()

        this.props.reload()
      })
  }

  deleteFav = list => {
    this.props.favLoader('Del')

    list.mType = list.id
    list.isShow = true
    list.image = list.image.substring(list.image.indexOf('/thumbnail'))
    delete list.isShowJP
    delete list.isNew
    delete list.isRecommend
    delete list.onClick
    delete list.isFav
    delete list.poster
    delete list.categoryIds
    delete list.id
    delete list.gType
    axios
      .post(`${process.env.REACT_APP_FAVORITE_LIST}/delFavList`, null, {
        params: {
          user: this.state.user,
          list,
        },
      })
      .then(res => {
        this.props.favLoader()

        this.props.reload()
      })
  }

  componentDidMount = () => {
    // this.getFavData()
  }

  render() {
    const isFav = this.state.game.isFav
    return (
      <Icon
        name={isFav ? 'heart' : 'heart outline'}
        onClick={e => {
          if (isFav) {
            /* TODO: Remove from favorite list */
            console.log('got here')
            this.deleteFav(this.props.game)
          } else {
            this.setFav(this.props.game)
          }
        }}
      />
    )
  }
}

export default FavGame
