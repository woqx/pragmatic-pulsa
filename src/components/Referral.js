import React, { useEffect, useContext, useState, useRef } from 'react'
import {
  Table,
  Card,
  Header,
  Container,
  Button,
  Icon,
  Tab,
  Grid,
  List,
  Segment,
  Input,
  Popup,
  Dropdown,
} from 'semantic-ui-react'
import api from '../api'
import { translate } from '../locales/Translate'
import { FacebookShareButton, TwitterShareButton, WhatsappShareButton, LineShareButton } from 'react-share'
import { FacebookIcon, TwitterIcon, LineIcon, WhatsappIcon } from 'react-share'
import CurrencyFormat from 'react-currency-format'
import { UserContext } from '../contexts/User'
import moment from 'moment'
import _ from 'lodash'

const getParent = async username => {
  const parent = api
    .getParentInfo(username)
    .then(json => json.json())
    .catch(e => console.error(e))

  return await parent
}

const getChildren = async username => {
  const children = api
    .getChildrenList(username)
    .then(json => json.json())
    .catch(e => console.error(e))
  return await children
}

const copyText = (text, { referralState, setRefState }) => {
  setRefState({ ...referralState, isOpen: true })
  const textField = document.createElement('textarea')
  textField.value = text
  textField.style.position = 'absolute'
  textField.style.left = '-9999px'
  textField.readOnly = true // avoid iOs keyboard opening
  textField.contentEditable = 'true'
  document.body.appendChild(textField)
  const isIOS = navigator.userAgent.match(/ipad|ipod|iphone/i)
  if (isIOS) {
    const range = document.createRange()
    range.selectNodeContents(textField)
    const selection = window.getSelection() // current text selection
    selection.removeAllRanges()
    selection.addRange(range)
    textField.setSelectionRange(0, text.length)
  } else {
    textField.select()
  }
  document.execCommand('copy')
  textField.remove()
  const timeout = setTimeout(() => {
    setRefState({ ...referralState, isOpen: false })
  }, 2500)
}

const Referral = () => {
  const withdrawButton = useRef(null)
  const ctx = useContext(UserContext)
  const [referralState, setRefState] = useState({
    parent: null,
    children: null,
    isOpen: false,
  })

  const getReferralsDetails = () => {
    let parent = false
    let children = false
    Promise.all([getParent(ctx.user.userName), getChildren(ctx.user.userName)]).then(resolve => {
      console.log({
        parent: resolve[0],
        children: resolve[1],
      })
      parent = resolve[0]
      children = resolve[1]
      setRefState({ ...referralState, parent: resolve[0], children: resolve[1] })
    })
  }

  useEffect(() => {
    withdrawButton.current.ref.disabled = true
    getReferralsDetails()

    if (moment().day() === 1 && moment(new Date()).hours() > 13 && moment(new Date()).hours() < 19) {
      withdrawButton.current.ref.disabled = false
    }
  }, [])

  const withdrawRef = async () => {
    withdrawButton.current.ref.disabled = true
    const _processWithdraw = await api
      .withdrawChildrenAmount(ctx.user.userName, Number.parseFloat(referralState.parent.balance))
      .then(json => json.json())
      .catch(e => {
        console.error(e)
        ctx.setErr(true, translate(ctx.preferredLocale, 'err.generalError'))
      })

    console.log(_processWithdraw)
    if (_processWithdraw === 'OK') {
      ctx.setErr(true, 'Withdraw referral success!')

      getReferralsDetails()
    }
  }

  return (
    <React.Fragment>
      <List divided verticalAlign='middle'>
        <List.Item as={Segment} style={{ padding: 16 }}>
          <List.Content floated='right'>
            <div style={{ display: 'flex', direction: 'row' }}>
              <Grid.Column floated>
                <FacebookShareButton
                  style={{ cursor: 'pointer' }}
                  quote={translate(ctx.preferredLocale, 'profile.referral.message')}
                  url={`http://${window.location.hostname}/register?ref=${ctx.user.userName}`}>
                  <FacebookIcon size={32} round={true}></FacebookIcon>
                </FacebookShareButton>
              </Grid.Column>
              <Grid.Column floated>
                <TwitterShareButton
                  style={{ cursor: 'pointer' }}
                  title={translate(ctx.preferredLocale, 'profile.referral.message')}
                  url={`https://${window.location.hostname}/register?ref=${ctx.user.userName}`}>
                  <TwitterIcon size={32} round={true}></TwitterIcon>
                </TwitterShareButton>
              </Grid.Column>
              <Grid.Column floated>
                <LineShareButton
                  style={{ cursor: 'pointer' }}
                  url={`${translate(ctx.preferredLocale, 'profile.referral.message')} https://${
                    window.location.hostname
                  }/register?ref=${ctx.user.userName}`}>
                  <LineIcon size={32} round={true}></LineIcon>
                </LineShareButton>
              </Grid.Column>
              <Grid.Column floated>
                <WhatsappShareButton
                  style={{ cursor: 'pointer' }}
                  title={translate(ctx.preferredLocale, 'profile.referral.message')}
                  url={`https://${window.location.hostname}/register?ref=${ctx.user.userName}`}>
                  <WhatsappIcon size={32} round={true}></WhatsappIcon>
                </WhatsappShareButton>
              </Grid.Column>
            </div>
          </List.Content>
          <List.Content>
            <Popup
              trigger={<Button>Share Link</Button>}
              on='click'
              content={'Copied'}
              open={referralState.isOpen}
              onOpen={() =>
                copyText(`https://${window.location.hostname}/register?ref=${ctx.user.userName}`, {
                  referralState,
                  setRefState,
                })
              }
              onClose={() => {
                setRefState({ ...referralState, isOpen: false })
              }}
              position='top right'
            />
          </List.Content>
        </List.Item>
      </List>

      {/* The input for withdraw */}
      <Input
        readOnly
        type='number'
        style={{ paddingBottom: 10, width: '160px' }}
        action={
          <Button
            ref={withdrawButton}
            color='teal'
            labelPosition='right'
            icon='arrow alternate circle right outline'
            content={translate(ctx.preferredLocale, 'pbs.withdraw')}
            onClick={e => {
              e.preventDefault()
              if (_.isUndefined(referralState.parent.balance)) {
                ctx.setErr(true, translate(ctx.preferredLocale, 'err.referralWithdrawNothing'))
                return
              }
              if (referralState.parent.balance <= 0) {
                ctx.setErr(true, translate(ctx.preferredLocale, 'err.referralWithdrawNothing'))
                return
              }
              withdrawRef()
            }}
          />
        }
        value={Math.round(referralState.parent && referralState.parent.balance) || 0}
      />
      {/* end input */}

      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell textAlign='center'>Username</Table.HeaderCell>
            <Table.HeaderCell textAlign='right'>Turnover</Table.HeaderCell>
            <Table.HeaderCell textAlign='right'>Win/Loss</Table.HeaderCell>
            <Table.HeaderCell textAlign='right'>Profit</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {referralState.children &&
            referralState.children.map(x => {
              return (
                <Table.Row>
                  <Table.Cell textAlign='center'>{x.username}</Table.Cell>
                  <Table.Cell textAlign='right'>
                    <CurrencyFormat
                      value={Number(Math.floor(100 * x.turnover) / 100).toFixed(2)}
                      displayType={'text'}
                      thousandSeparator={true}
                      prefix={'฿'}
                    />
                  </Table.Cell>
                  <Table.Cell textAlign='right'>
                    <CurrencyFormat
                      value={Number(Math.floor(100 * x.winloss) / 100).toFixed(2)}
                      displayType={'text'}
                      thousandSeparator={true}
                      prefix={'฿'}
                    />
                  </Table.Cell>
                  <Table.Cell textAlign='right'>
                    <CurrencyFormat
                      value={Number(Math.floor(100 * x.profit) / 100).toFixed(2)}
                      displayType={'text'}
                      thousandSeparator={true}
                      prefix={'฿'}
                    />
                  </Table.Cell>
                </Table.Row>
              )
            })}
        </Table.Body>
      </Table>
    </React.Fragment>
  )
}

export default Referral
