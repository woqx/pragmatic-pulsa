import React, { Component } from 'react'
import { Icon, Container, Segment, Grid, GridColumn, List, Image } from 'semantic-ui-react'
import Translate from '../locales/Translate'
import { LocaleContext } from '../contexts/Locale'
import thFlag from '../assets/images/thailand.svg'
import enFlag from '../assets/images/united-kingdom.svg'
import idFlag from '../assets/images/indonesia.svg'
import { faWhatsapp, faLine, faWeixin, faSkype, faTelegram } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import api from '../api'

const providers = []
export default class Footer extends Component {
  render() {
    return (
      <div class='ui inverted vertical footer segment' className='customFooter' style={{ height: '100%' }}>
        <div class='link-center'></div>

        <Container>
          <Grid stackable>
            <Grid.Column width={12} textAlign='left'>
              <Segment inverted compact>
                <List horizontal>
                  {['id', 'en', 'th'].map(lang => {
                    return (
                      <List.Item>
                        <LocaleContext.Consumer>
                          {({ preferredLanguage, changeLanguage }) => (
                            <Image
                              size='mini'
                              src={lang === 'en' ? enFlag : lang === 'th' ? thFlag : idFlag}
                              style={{ cursor: 'pointer' }}
                              onClick={() => changeLanguage(lang)}
                              positive={preferredLanguage === lang}
                            />
                          )}
                        </LocaleContext.Consumer>
                      </List.Item>
                    )
                  })}
                </List>
              </Segment>
              <Segment inverted>
                <p>PlayPragmatic - Agen Slot Online Pragmatic Play Indonesia</p>
                <p>
                  PlayPragmatic adalah Agen Slot Online Pragmatic Play Indonesia Terbaru dan Terpercaya. Dengan
                  bergabung bersama kami, anda dapat memainkan semua games tersebut dengan 1 user id dengan minimal
                  deposit Rp 10.000 dan withdraw Rp 50.000 saja.
                </p>

                <p>
                  Kami menerima pembayaran melalui :
                  <List bulleted>
                    <List.Item>Semua bank lokal indonesia</List.Item>
                    <List.Item>Pulsa (Telkomsel & XL).</List.Item>
                    <List.Item>E-Money (Dana, Ovo dan Gopay).</List.Item>
                  </List>
                </p>

                <p>
                  Ayo bergabung sekarang bersama PlayPragmatic dan dapatkan berbagai keuntungan yang sangat besar
                  disini!
                </p>

                <Grid columns='5' style={{ marginTop: '30px' }}>
                  {providers &&
                    providers.map(p => (
                      <Grid.Column className='bg-provider-footer'>
                        <Image
                          size={api.isAppMobile ? 'tiny' : ''}
                          src={`https://ik.imagekit.io/xr5d8qn155ac5/Provider-logo/${p}.png`}
                        />
                      </Grid.Column>
                    ))}
                </Grid>
              </Segment>
            </Grid.Column>
            <Grid.Column width={4}>
              <Segment inverted>
                <h2>Hubungi kami</h2>
                <List inverted verticalAlign='middle'>
                  <List.Item>
                    <FontAwesomeIcon icon={faWhatsapp} size='2x' style={{ paddingRight: '5px' }} />{' '}
                    <a href='https://wa.me/855962952124' rel='nofollow'>
                      +855962952124
                    </a>
                  </List.Item>
                  <List.Item>
                    <FontAwesomeIcon icon={faLine} size='2x' style={{ paddingRight: '5px' }} />
                    playpragmatic
                  </List.Item>
                  <List.Item>
                    <FontAwesomeIcon icon={faTelegram} size='2x' style={{ paddingRight: '5px' }} />{' '}
                    <a href='https://t.me/+855962952124' rel='nofollow'>
                      +855962952124
                    </a>
                  </List.Item>
                </List>
              </Segment>
              <Segment inverted>
                <List>
                  <List.Item as='a' href='/promo'>
                    <Icon name='gift' color='purple' />
                    Promo
                  </List.Item>
                  <List.Item>Game Slots</List.Item>
                  <List.Item>Slot Online</List.Item>
                  <List.Item>Tembak Bola</List.Item>
                  <List.Item>Puzzle Bubble</List.Item>
                  <List.Item>Ular Tangga</List.Item>
                  <List.Item>Tembak Ikan</List.Item>
                  <List.Item>Keno</List.Item>
                  <List.Item>Bingo</List.Item>
                  <List.Item>Livecasino</List.Item>
                </List>
              </Segment>
            </Grid.Column>
          </Grid>
        </Container>

        <div class='ui center aligned container'>
          <div class='ui inverted section divider'></div>
          <a href='/'>PLAYPRAGMATIC</a>
          <small>ⓒ 2019. All Right Reserved | 18+ (Powered by daVinci Gaming)</small>
          <div class='ui horizontal inverted small divided link list'></div>
        </div>
      </div>
    )
  }
}
