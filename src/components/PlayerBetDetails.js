import React, { useState, useEffect, useRef } from 'react'
import { useQuery, ApolloClient, InMemoryCache, HttpLink } from '@apollo/client'
import api from '../api'
import { Table, Button, Icon, Dropdown, Dimmer, Loader } from 'semantic-ui-react'
import mtx from 'moment-timezone'
import CurrencyFormat from 'react-currency-format'
import MajaSportTicketDetails from '../pbd_components/MajaSportTicketDetails'
import Translate, { translate } from '../locales/Translate'

const formatToNumber = num => Number(num.replace(',', ''))

// Hook
function usePrevious(value) {
  // The ref object is a generic container whose current property is mutable ...
  // ... and can hold any value, similar to an instance property on a class
  const ref = useRef()

  // Store current value in ref
  useEffect(() => {
    ref.current = value
  }, [value]) // Only re-run if value changes

  // Return previous value (happens before update in useEffect above)
  return ref.current
}

const PlayerBetDetails = props => {
  const { dateStart, dateEnd, playerName, agentName, offset, first } = props
  const [page, setPage] = useState(offset)
  const [f, setF] = useState(first)
  const [pbd, setPbd] = useState({ Lines: {}, totalPages: [] })
  const [isLoading, setLoading] = useState(false)
  const prevPage = usePrevious(page)

  useEffect(() => {
    setLoading(true)
    const httpLink = new HttpLink({ uri: process.env.REACT_APP_GRAPHQL_URL })
    const repClient = new ApolloClient({
      link: httpLink,
      cache: new InMemoryCache(),
    })

    repClient
      .query({
        query: api.PBDQuery,
        variables: {
          dateStart,
          dateEnd,
          playerName,
          agentName,
          offset: page,
          first: f,
        },
      })
      .then(res => {
        const totalPage = []
        for (let i = 0; i < res.data.playerBetDetails.count / 10; i++) {
          totalPage.push(i)
        }
        setPbd({ Lines: res.data.playerBetDetails, totalPages: totalPage })
        setLoading(false)
      })
  }, [page])

  return (
    <React.Fragment>
      <Button
        icon
        labelPosition='left'
        style={{ display: 'inline-block', verticalAlign: 'middle' }}
        onClick={e => {
          e.preventDefault()
          props.showPBD()
        }}>
        <Icon name='backward' />
        Back
      </Button>
      <div style={{ float: 'right' }}>
        <span style={{ color: 'black' }}>Page : </span>
        <Dropdown
          options={pbd.totalPages.map(g => {
            return {
              key: g,
              text: g + 1,
              value: g,
            }
          })}
          value={page / 10}
          placeholder='Select Page'
          selection
          onChange={(e, { value }) => {
            setPage(value * 10)
          }}
        />
      </div>
      <Table striped>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>
              <Translate string='pbd.date' />
            </Table.HeaderCell>
            <Table.HeaderCell>
              <Translate string='pbd.game' />
            </Table.HeaderCell>
            <Table.HeaderCell>
              <Translate string='pbd.stake' />
            </Table.HeaderCell>
            <Table.HeaderCell>
              <Translate string='pbd.netWin' />
            </Table.HeaderCell>
            <Table.HeaderCell>
              <Translate string='pbd.afterBalance' />
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          <Dimmer active={isLoading}>
            <Loader content='Loading' />
          </Dimmer>
          {pbd &&
            pbd.Lines &&
            pbd.Lines.playerLines &&
            pbd.Lines.playerLines.map(l => (
              <Table.Row>
                <Table.Cell>{mtx.tz(l.transactionTime, 'Asia/Bangkok').format('DD-MM-YYYY HH:mm:ss')}</Table.Cell>
                <Table.Cell>
                  {(() => {
                    if (l.providerName === 'MJS') {
                      return (
                        <MajaSportTicketDetails
                          match={JSON.parse(l.type)}
                          ticketInfos={JSON.parse(l.bets)}
                          betId={l.transaction}
                        />
                      )
                    }
                    return l.type
                  })()}
                  <br />
                  <small>{l.transaction}</small>
                </Table.Cell>
                <Table.Cell>
                  <CurrencyFormat
                    value={Number(Math.floor(100 * l.stake) / 100).toFixed(2)}
                    displayType={'text'}
                    thousandSeparator={true}
                    prefix={'Rp'}
                  />
                </Table.Cell>
                <Table.Cell positive={formatToNumber(l.winLose) > 0} negative={formatToNumber(l.winLose) < 0}>
                  <CurrencyFormat
                    value={Number(Math.floor(100 * l.winLose) / 100).toFixed(2)}
                    displayType={'text'}
                    thousandSeparator={true}
                    prefix={'Rp'}
                  />
                </Table.Cell>
                <Table.Cell>
                  <CurrencyFormat value={l.afterBalance} displayType={'text'} thousandSeparator={true} prefix={'Rp'} />
                </Table.Cell>
              </Table.Row>
            ))}
        </Table.Body>
      </Table>
    </React.Fragment>
  )
}

export default PlayerBetDetails
