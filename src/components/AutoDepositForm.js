import React, { useState, useContext, useRef } from 'react'
import { Button, Header, Image, List, Message, Popup, Segment, Icon, Grid, Divider } from 'semantic-ui-react'
import Translate, { translate } from '../locales/Translate'
import Axios from 'axios'
import { UserContext } from '../contexts/User'

const getBankIcon = bankCode => {
  switch (bankCode) {
    case 'KTB':
      return 'https://ik.imagekit.io/xr5d8qn155ac5/KTB_xVTJvLzpO.jpg'
    case 'SCB':
      return 'https://i0.wp.com/www.myshineyhineythailand.com/wp-content/uploads/2019/01/scb-icon.png'
    case 'KRUNGSRI':
      return 'https://ik.imagekit.io/xr5d8qn155ac5/krungsri_6s5LNVCUY.jpg'
  }
}

const ScrapperButton = ({ bankAccount, ctx }) => {
  let scraperURL = false
  const [disableMe, setDisableMe] = useState(false)

  if (bankAccount.bankCode === 'KRUNGSRI') {
    scraperURL = 'https://asia-east2-siam44.cloudfunctions.net/checkStatus'
  } else if (bankAccount.bankCode === 'SCB') {
    scraperURL = 'https://asia-east2-siam44.cloudfunctions.net/checkSCBBank'
  }
  return (
    <Button
      disabled={disableMe}
      loading={disableMe}
      content={
        <React.Fragment>
          <Icon name='arrow right' style={{ marginRight: '5px' }} />
          <Translate string='profile.checkAutoBank' />
          <Icon name='arrow left' style={{ marginLeft: '5px' }} />
        </React.Fragment>
      }
      color='google plus'
      fluid
      onClick={async () => {
        setDisableMe(prevState => !prevState)
        const bankStatus = await Axios.post(scraperURL)
          .then(x => {
            return x.data
          })
          .catch(e => console.log(e))

        if (bankStatus.Status === 'Idle') {
          await Axios.post('https://asia-east2-siam44.cloudfunctions.net/checkBank')
            .then(x => {
              setDisableMe(prevState => !prevState)

              console.log(x)
            })
            .catch(err => {
              console.error(err)
              setDisableMe(prevState => !prevState)
            })
        } else {
          ctx.setErr(true, 'Service is running, please wait 2 Minutes')
          setDisableMe(prevState => !prevState)
        }
      }}
    />
  )
}

const AutoDepositForm = ({ bankAccount, howTos }) => {
  const [isOpen, setIsOpen] = useState(false)
  const ctx = useContext(UserContext)
  let intervalRef = useRef(null)

  const handleOpen = bankAccount => {
    setIsOpen(true)
    const textField = document.createElement('textarea')
    textField.value = bankAccount
    textField.style.position = 'absolute'
    textField.style.left = '-9999px'
    textField.readOnly = true // avoid iOs keyboard opening
    textField.contentEditable = 'true'
    document.body.appendChild(textField)
    const isIOS = navigator.userAgent.match(/ipad|ipod|iphone/i)
    if (isIOS) {
      const range = document.createRange()
      range.selectNodeContents(textField)
      const selection = window.getSelection() // current text selection
      selection.removeAllRanges()
      selection.addRange(range)
      textField.setSelectionRange(0, bankAccount.length)
    } else {
      textField.select()
    }
    document.execCommand('copy')
    textField.remove()
    intervalRef = setTimeout(() => {
      setIsOpen(false)
    }, 2500)
  }

  const handleClose = () => {
    setIsOpen(false)
    clearTimeout(intervalRef)
  }

  //   console.log(bankAccounts)
  // {bankName: "bank.KRUNGSRI", accountNumber: "12345600", accountName: "", bankCode: "KRUNGSRI", isShow: true, …}
  return (
    <div>
      <Message warning>
        <Header size='tiny'>
          {' '}
          <Translate string='bank.Notice' />
        </Header>
      </Message>
      <List divided verticalAlign='middle'>
        <List.Item as={Segment} style={{ padding: 16 }}>
          <List.Content floated='right'>
            <Popup
              trigger={<Button>Copy</Button>}
              on='click'
              content={'Copied'}
              open={isOpen}
              onOpen={() => handleOpen(bankAccount.accountNumber)}
              onClose={() => handleClose()}
              position='top right'
            />
          </List.Content>
          <Image avatar src={getBankIcon(bankAccount.bankCode)} />
          <List.Content>
            <div style={{ color: 'black', fontWeight: 'bold' }}>
              <Translate string={bankAccount.bankName} />
            </div>
            <div style={{ color: 'black' }}>{bankAccount.accountNumber}</div>
          </List.Content>
        </List.Item>
      </List>

      <ScrapperButton bankAccount={bankAccount} ctx={ctx} />
      <Message warning>
        <Header>
          <Icon name='bullhorn' />
          <Translate string='bank.howToBank' />
        </Header>
        <List bulleted>
          {howTos.map(step => (
            <List.Item>
              <Translate string={step} />
            </List.Item>
          ))}
        </List>
        <small>
          <i>
            <Translate string='bank.footerWarning' />
          </i>
        </small>
      </Message>
    </div>
  )
}

export default AutoDepositForm
