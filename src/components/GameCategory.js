import React, { Component } from 'react'
import { Image, Label } from 'semantic-ui-react'
import Translate, { translate } from '../locales/Translate'

const isAppMobile = /Mobi/.test(navigator.userAgent)
const GameCategory = ({ isActive, activeImage, image, name, onClick, ctx }) => (
  <div style={{ textAlign: 'center', borderBottom: isActive ? 2 : 0 }} onClick={onClick}>
    <Image
      onMouseOver={e => (isActive ? null : (e.currentTarget.src = activeImage))}
      onMouseOut={e => (isActive ? null : (e.currentTarget.src = image))}
      src={isActive ? activeImage : image}
      size='mini'
      className='catImage'
      centered
      // style={isAppMobile ? { width: '50px' } : { margin: 'auto', width: '70px' }}
    />
    <div style={{ paddingTop: '5px' }}>
      <Label className={isActive ? 'catShiny' : 'catLabel'}>
        <Translate string={name} />
      </Label>
    </div>
  </div>
)
export default GameCategory
