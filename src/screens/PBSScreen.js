import React, { useContext, useEffect, useState } from 'react'
import { gql, ApolloClient, InMemoryCache, HttpLink, ApolloLink } from '@apollo/client'
import { UserContext } from '../contexts/User'
import {
  Table,
  Card,
  Header,
  Container,
  Button,
  Icon,
  Tab,
  Grid,
  List,
  Segment,
  Input,
  Popup,
  Dropdown,
  Dimmer,
  Loader,
} from 'semantic-ui-react'
import Translate, { translate } from '../locales/Translate'
import CurrencyFormat from 'react-currency-format'
import PlayerBetDetails from '../components/PlayerBetDetails'

import mtx from 'moment-timezone'
import moment from 'moment'
import api from '../api'
import Referral from '../components/Referral'

const weekOptions = [
  {
    key: 'lastweek',
    text: 'Last Week',
    value: 'lastweek',
  },
  {
    key: 'thisweek',
    text: 'This Week',
    value: 'thisweek',
  },
]

const getWeekDay = () => {
  const prevMonday = mtx.tz('Asia/Bangkok').day('Monday')
  const midnightBk = mtx.tz(prevMonday.format('YYYY-MM-DD'), 'Asia/Bangkok')
  // return { dateFrom: midnightBk.toDate(), dateTo: false };
  return {
    dateFrom: moment(midnightBk).toDate(),
    dateTo: midnightBk.add(+7, 'd').toDate(),
  }
}

const getLastWeek = () => {
  const prevMonday = mtx.tz('Asia/Bangkok').day('Monday')
  const midnightBk = mtx.tz(prevMonday.format('YYYY-MM-DD'), 'Asia/Bangkok')
  return {
    dateFrom: moment(midnightBk)
      .add(-7, 'd')
      .toDate(),
    dateTo: midnightBk.add(-2).toDate(),
  }
}

const _renderTransactions = playerTransactions => (
  <Table compact>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Date</Table.HeaderCell>
        <Table.HeaderCell>Type</Table.HeaderCell>
        <Table.HeaderCell textAlign='right'>Amount</Table.HeaderCell>
        <Table.HeaderCell textAlign='right'>Status</Table.HeaderCell>
      </Table.Row>
    </Table.Header>

    <Table.Body>
      {playerTransactions === 'Loading' && (
        <Table.Row>
          <Table.Cell colspan='4'>Loading</Table.Cell>
        </Table.Row>
      )}
      {playerTransactions !== 'Loading' &&
        playerTransactions.playerLines.map(x => (
          <Table.Row>
            <Table.Cell collapsing>
              {mtx.tz(x.transactionTime, 'Asia/Bangkok').format('DD-MM-YYYY HH:mm:ss')}
            </Table.Cell>
            <Table.Cell>{x.type.toUpperCase()}</Table.Cell>
            <Table.Cell textAlign='right'>{x.winLose}</Table.Cell>
            <Table.Cell textAlign='right'>{x.status}</Table.Cell>
          </Table.Row>
        ))}
    </Table.Body>
  </Table>
)

const _renderPBS = (ctx, pbs, { PBD, setShowPBD }) => (
  <Table striped>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>
          {/* <Dropdown
              fluid
              selection
              options={weekOptions}
              value={selectedWeek}
              onChange={handleIntervalChange}
            /> */}
        </Table.HeaderCell>

        <Table.HeaderCell textAlign='center'>
          <Translate string={'pbs.turnover'} />
        </Table.HeaderCell>
        <Table.HeaderCell textAlign='center'>
          <Translate string={'pbs.winloss'} />
        </Table.HeaderCell>
      </Table.Row>
    </Table.Header>

    <Table.Body>
      {pbs === 'Loading' && (
        <Table.Row>
          <Table.Cell colspan='3'>Loading</Table.Cell>
        </Table.Row>
      )}
      {pbs !== 'Loading' &&
        pbs.map(x => (
          <Table.Row>
            <Table.Cell collapsing>{x.date}</Table.Cell>
            <Table.Cell textAlign='right' collapsing>
              <CurrencyFormat
                value={Number(Math.floor(100 * x.validTurnover) / 100).toFixed(2)}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'Rp'}
              />
            </Table.Cell>
            <Table.Cell style={{ cursor: 'pointer' }} textAlign='right' collapsing selectable>
              <a
                href='{null}'
                onClick={e => {
                  e.preventDefault()
                  const pbdDateStart = mtx
                    .tz(x.date, 'Asia/Bangkok')
                    .startOf('day')
                    .format('YYYY-MM-DDTHH:mm:ss')
                  const pbdDateEnd = mtx
                    .tz(x.date, 'Asia/Bangkok')
                    .endOf('day')
                    .format('YYYY-MM-DDTHH:mm:ss')
                  setShowPBD({
                    pbdDateStart,
                    pbdDateEnd,
                    showPBD: true,
                  })
                }}>
                <CurrencyFormat
                  value={Number(Math.floor(100 * x.playerWinLoss) / 100).toFixed(2)}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'Rp'}
                />
              </a>
            </Table.Cell>
          </Table.Row>
        ))}
    </Table.Body>
  </Table>
)

const _playerBetDetails = (ctx, { PBD, setShowPBD }) => (
  <PlayerBetDetails
    dateStart={PBD.pbdDateStart}
    dateEnd={PBD.pbdDateEnd}
    playerName={ctx.user.userName}
    agentName={api.agentName}
    offset={0}
    first={10}
    showPBD={() => {
      setShowPBD({
        ...PBD,
        showPBD: false,
      })
    }}
  />
)

const PBSScreen = () => {
  const ctx = useContext(UserContext)
  const [pbs, setPBS] = useState('Loading')
  const [witdep, setWitDep] = useState('Loading')
  const [PBD, setShowPBD] = useState({ pbdDateStart: '', pbdDateEnd: '', showPBD: false })
  const [isLoading, setLoading] = useState(true)

  const queryES = () => {
    const week = getWeekDay()
    const dateFrom = mtx
      .tz(new Date(), 'Asia/Bangkok')
      .subtract(3, 'days')
      .format('YYYY-MM-DDTHH:mm:ss')
    const httpLink = new HttpLink({ uri: process.env.REACT_APP_GRAPHQL_URL })
    const repclient = new ApolloClient({
      link: httpLink,
      cache: new InMemoryCache(),
    })

    repclient
      .query({
        query: gql`
          ${api.queryPlayersBet}
        `,
        variables: {
          playerName: ctx.user.userName,
          dateStart: week.dateFrom,
          dateEnd: week.dateTo,
          transactionStartDate: dateFrom,
        },
      })
      .then(res => {
        console.log('query GQL', res)
        if (res.data) {
          setPBS(res.data.pbs)
          setWitDep(res.data.playerTransactions)
          setLoading(false)
        }
      })
      .catch(e => console.error('GraphQL error', e))
  }

  useEffect(() => {
    queryES()
  }, [])

  const panes = [
    {
      menuItem: {
        content: <Translate string='pbs.transactions' />,
      },
      render: () => (
        <Tab.Pane>
          <Dimmer active={isLoading}>
            <Loader content='Loading' />
          </Dimmer>
          {_renderTransactions(witdep)}
        </Tab.Pane>
      ),
    },
    {
      menuItem: {
        content: <Translate string='pbs.betSummary' />,
      },
      render: () => (
        <Tab.Pane>
          <Dimmer active={isLoading}>
            <Loader content='Loading' />
          </Dimmer>
          {!PBD.showPBD ? _renderPBS(ctx, pbs, { PBD, setShowPBD }) : _playerBetDetails(ctx, { PBD, setShowPBD })}
        </Tab.Pane>
      ),
    },
    {
      menuItem: {
        content: <Translate string='pbs.Referral' />,
      },
      render: () => (
        <Tab.Pane>
          <Referral />
        </Tab.Pane>
      ),
    },

    // {
    //   menuItem: {
    //     content: <Translate string='pbs.Referral' />,
    //   },
    //   render: () => <Tab.Pane>{this._renderReferral()}</Tab.Pane>,
    // },
  ]
  return (
    <Container className='containerExpand'>
      <Card className='custom-content'>
        <Header as='h2' style={{ color: '#ccc', paddingBottom: '16px' }} className='form-header' textAlign='center'>
          <Translate string='pbs.heading' />
        </Header>

        <Tab
          panes={panes}
          onTabChange={(e, data) => {
            if (isLoading) {
              return
            }
            setLoading(true)
            queryES()
          }}
        />
      </Card>
    </Container>
  )
}

export default PBSScreen
