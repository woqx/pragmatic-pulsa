import React, { Component } from 'react'
import { UserContext } from '../contexts/User'
import Translate, { translate } from '../locales/Translate'
import { Container, Card, Image } from 'semantic-ui-react'
import api from '../api'

class SportScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      provider: this.props.provider,
    }
  }

  launchMJS = async () => {
    var mjsTab = window.open('', `MJS-${new Date().getTime()}`, 'width=1240,height=700,left=0,top=0')
    await api
      .enterMJS()
      .then(x => x.json())
      .then(res => {
        mjsTab.location.href = res.data.url
        //return res.data.url
      })
      .catch(err => {
        console.log(err)
      })
  }

  launchSBO = async () => {
    var sboTab = window.open('', `SBO-${new Date().getTime()}`, 'width=1240,height=700,left=0,top=0')
    await api
      .loginSBO(api.getCurrentUser())
      .then(x => x.json())
      .then(x => {
        sboTab.location.href = x.result
      })
      .catch(err => {
        console.log(err)
      })
  }

  componentDidMount = () => {
    this.loads()
  }

  componentDidUpdate = prevProps => {
    if (this.props.x !== prevProps.x) {
      this.setState(
        {
          provider: this.props.provider,
        },
        () => this.loads(),
      )
    }
  }
  loads = () => {
    const { user, setErr, preferredLocale } = this.context
    const { provider } = this.state
    console.log(provider)
    if (user) {
      if (!user.isPromo) {
        if (provider === 'SBO') this.launchSBO()
        if (provider === 'MJS') this.launchMJS()
      } else {
        setErr(true, translate(preferredLocale, 'err.noAccessPromo'))
      }
    }
  }

  render() {
    const { user } = this.context

    return (
      // <Container id='container' className='containerExpand'>
      <div style={{ marginTop: '20px' }}>
        {user ? (
          <Container className='containerExpand' textAlign='center' style={{ marginTop: 20, marginBottom: 10 }}>
            <Card>
              <Card.Content>
                <Card.Header>
                  <Translate string='sports.openInNewTab' />
                </Card.Header>
                <Card.Meta>
                  <Image src={`${process.env.REACT_APP_IMAGE_URL}/thumbnail/MJS/majagames_black.png`} size='medium' />
                </Card.Meta>
              </Card.Content>
            </Card>
          </Container>
        ) : (
          <Container>
            <h1>
              <Translate string='err.PleaseLogin' />
            </h1>
          </Container>
        )}
      </div>
    )
  }
}
SportScreen.contextType = UserContext
export default SportScreen
