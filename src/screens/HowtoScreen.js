import React from 'react'
import butter from '../butter-cms'
import { Helmet } from 'react-helmet'

export default class extends React.Component {
  state = {
    data: {
      fields: {
        customer_logos: []
      }
    }
  }
  async componentDidMount () {
    const { match } = this.props
    const resp = await butter.page.retrieve('*','how-to')
    this.setState(resp.data)
  }
  render () {
    const { fields } = this.state.data

    return (
      <div>
        <Helmet>
          <title>{fields.seo_title}</title>
          <meta property='og:title' content={fields.facebook_open_graph_title} />
        </Helmet>
        <div dangerouslySetInnerHTML={{ __html: fields.body }} />
      </div>
    )
  }
}