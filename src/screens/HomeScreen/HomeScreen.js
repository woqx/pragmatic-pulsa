import React, { useState, useEffect, useContext } from 'react'
import {
  Card,
  Container,
  Grid,
  Image,
  Label,
  Loader,
  Icon,
  Dimmer,
  Input,
  Button,
  Dropdown,
  Divider,
} from 'semantic-ui-react'
import api from '../../api'
import GameCategory from '../../components/GameCategory'
import { UserContext } from '../../contexts/User'
import gamesTransformer from '../../transformers/main'
import { categoryCodes } from '../../config/games/index'
import Translate, { translate } from '../../locales/Translate'
import { Helmet } from 'react-helmet'
import jwt from 'jwt-decode'
import FavGame from '../../components/FavGame'
import LazyLoad from 'react-lazyload'
import axios from 'axios'
import GamesList from './GamesList'
import SlidersCustom from './Sliders'
import _ from 'lodash'
import '../../assets/Providers-dropdown.css'
import SpecialGames from './SpecialGames'

const HomeScreen = () => {
  const urlSearchParams = new URLSearchParams(window.location.search)
  const [homeState, setHomeState] = useState({
    showSport: false,
    isLoading: false,
    allGames: null,
    searchTerm: '',
    filterProvider: 'All',
    favLoader: false,
  })
  const [catId, setCatId] = useState(
    urlSearchParams.get('cat') ? urlSearchParams.get('cat') : Number(localStorage.getItem('activeCategory')) || 3,
  )

  const ctx = useContext(UserContext)

  const getGames = async first => {
    const GameResponse = await api.getGamesList()
    if (GameResponse.status === '500') {
      return false
    }
    const GamesJson = await GameResponse.json()
    let Games = []
    GamesJson.games.forEach(x => {
      x.list.forEach(y => {
        const gameItem = gamesTransformer(y, x.gType, ctx)
        if (catId === 3 || catId === 7) return Games.push(gameItem)
        if (catId === 1 && gameItem.isPromo) {
          Games.push(gameItem)
          return
        }
        if (_.includes(gameItem.categoryIds, catId) && !gameItem.isNew && !gameItem.isRecommend) {
          const poms = window.proHigh[process.env.REACT_APP_MAIN_PARENT]
          for (var i = 0; i < poms.length; i++) {
            if (gameItem.provider === poms[i] && catId === 1) return false
          }
          Games.push(gameItem)
        }
      })
    })

    const uniq_providers = _.uniqBy(Games, function(o) {
      return o.provider
    })
    uniq_providers.sort((a, b) => (a.provider > b.provider ? 1 : -1))
    const providers = uniq_providers
      .filter(up => {
        return up.provider !== undefined
      })
      .map(up => {
        const p = {
          key: up.provider,
          value: up.provider,
          text: window.pName(up.provider),
          image: { avatar: true, src: `${process.env.REACT_APP_IMAGE_URL}/Provider-logo/v2/${up.provider}.png` },
        }
        return p
      })

    providers.unshift({ key: 'All', value: 'All', text: 'All' })

    if (ctx.user && first) {
      await axios
        .post(`${process.env.REACT_APP_FAVORITE_LIST}/getFavList`, null, {
          params: {
            user: ctx.user.userName,
          },
        })
        .then(res => {
          res.data.list.map(x => {
            const favGames = JSON.parse(x)
            favGames.isFav = true
            const gameItem = gamesTransformer(favGames, 97, ctx)
            Games.push(gameItem)
          })
        })
        .catch(e => console.error(e))
    }

    setHomeState(prevState => ({
      ...homeState,
      Games,
      allGames: first ? [...Games] : Games,
      isLoading: false,
      showSport: false,
      providers,
      filterProvider: first ? 'All' : prevState.filterProvider,
    }))
  }

  const handleGameCategorySelection = async id => {
    if (id === catId) return false
    localStorage.setItem('activeCategory', id)
    urlSearchParams.set('cat', id)
    setHomeState({ ...homeState, isLoading: true, showSport: false, filterProvider: 'All' })
    setCatId(id)
  }

  const setFavLoader = mode => {
    setHomeState(prevState => ({
      ...homeState,
      favLoader: !prevState.favLoader,
      favMode: mode,
    }))
  }

  const changeProvider = async (e, data) => {
    if (data.value === 'All') {
      getGames(true)
    }
    const fp = await homeState.Games.filter(x => {
      return x.provider === `${data.value}`
    })
    setHomeState({ ...homeState, allGames: fp, filterProvider: data.value })
  }

  useEffect(() => {
    if (ctx.user && catId === 1) {
      handleGameCategorySelection(3)
    }
    if (catId === 7) {
      getGames(true)
    }

    getGames(false)
  }, [catId])

  const selectedGames = homeState.searchTerm
    ? _.filter(homeState.allGames, x => {
        if (!x) return false

        return x.name.toLowerCase().search(homeState.searchTerm) !== -1
      })
    : _.filter(homeState.allGames, x => {
        if (catId === 1) return x
        return _.includes(x.categoryIds, catId)
      })

  return (
    <div>
      <UserContext.Consumer>
        {({ user, sitesInfo }) => {
          let header = false
          if (sitesInfo && sitesInfo.header) {
            header = JSON.parse(sitesInfo.header)
          }
          return (
            <React.Fragment>
              <Helmet>
                <title>{sitesInfo && sitesInfo.landingpage && sitesInfo.landingpage.title}</title>
                <meta
                  name='description'
                  content={sitesInfo && sitesInfo.landingpage && sitesInfo.landingpage.description}
                />
                {/* <meta name='google-site-verification' content={header['google-site-verification']} /> */}
                <link rel='canonical' href={header.canonical} />
              </Helmet>
              {/* BANNER AREA */}
              {!user && <SlidersCustom banners={sitesInfo && sitesInfo.bannerArray} bannerSlide={true} />}
            </React.Fragment>
          )
        }}
      </UserContext.Consumer>

      <div style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)', marginTop: '10px', marginBottom: '20px' }}>
        <Container style={{ position: 'relative' }}>
          <Grid>
            <Grid.Row style={{ padding: 0 }}>
              <Grid.Column style={{ padding: '0', paddingTop: '0px' }}>
                <div className='catCSS'>
                  <SlidersCustom
                    categorySlide={true}
                    activeGameCategory={catId}
                    categoryCodes={categoryCodes}
                    changeCategory={handleGameCategorySelection}
                    ctx={ctx}
                  />
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      </div>
      {/*END  CATEGORY AREA */}

      {/* Special games ares */}
      <SpecialGames />
      {/* End Special */}

      {/* GamesList Area */}
      {(() => {
        if (homeState.isLoading) {
          return (
            <Container className='containerExpand'>
              <Loader active inline='centered' size='big'>
                Loading
              </Loader>
            </Container>
          )
        }

        return (
          <React.Fragment>
            <Container className='posterGames' style={{ marginBottom: 10 }}>
              <div>
                <GamesList allGames={homeState.allGames} G={homeState.Games} activeGameCategory={catId} ctx={ctx} />
              </div>
            </Container>

            <Container className='containerExpand' style={{ marginTop: 5, marginBottom: 20 }}>
              {catId !== 3 && catId !== 7 ? (
                <React.Fragment>
                  <Grid stackable columns={2}>
                    <Grid.Column>
                      <Input
                        icon
                        placeholder={translate(ctx.preferredLocale, 'home.search')}
                        fluid
                        onChange={e => setHomeState({ ...homeState, searchTerm: e.target.value.toLocaleLowerCase() })}>
                        <input value={homeState.searchTerm} />
                        <Icon name='search' />
                      </Input>
                    </Grid.Column>
                    <Grid.Column>
                      <Dropdown
                        inverted
                        className='pvd-dropdown'
                        placeholder='Filter by providers'
                        fluid
                        selection
                        options={homeState.providers}
                        value={homeState.filterProvider}
                        onChange={changeProvider}
                      />
                    </Grid.Column>
                  </Grid>
                  <Divider />
                </React.Fragment>
              ) : (
                ''
              )}
              <p></p>

              <div>
                {selectedGames && (
                  <Grid doubling columns={window.innerWidth < 1024 ? '10' : '5'}>
                    {selectedGames.map((y, idx) => {
                      if (y.isRecommend) {
                        return false
                      }
                      if (y.isNew) {
                        return false
                      }

                      const poms = window.proHigh[process.env.REACT_APP_MAIN_PARENT]
                      for (var i = 0; i < poms.length; i++) {
                        if (y.provider === poms[i] && (y.gType === 0 || y.gType === 99) && catId !== 1) return false
                        // if (y.provider === poms[i] && catId === 1) return false
                      }

                      return (
                        <Grid.Column className={'gridGames'} key={idx}>
                          <Card className={'imgList'}>
                            <div
                              className='logo-providers-sm'
                              style={{
                                zIndex: '1',
                                position: 'absolute',
                                // height: 'fit-content',
                                backgroundColor: 'rgba(0, 0, 0, 0.65)',
                                padding: '0px 10px 0px 10px',
                                top: '4px',
                                left: '5px',
                                width: '85px',
                                textOverflow: 'ellipsis',
                                whiteSpace: 'nowrap',
                                overflow: 'hidden',
                              }}>
                              <span style={{ fontSize: api.isAppMobile ? '0.6rem' : '1rem' }}>
                                {window.pName(y.provider)}
                              </span>
                            </div>
                            <LazyLoad>
                              <Image onClick={() => y.onClick(y, ctx.user, ctx)} src={y.image} />
                            </LazyLoad>

                            <Label className={'gameLabel'}>
                              {(() => {
                                if (ctx.user) {
                                  if (catId === 3) {
                                    if (y.isFav) {
                                      return (
                                        <FavGame
                                          game={y}
                                          reload={() => {
                                            getGames(true)
                                          }}
                                          favLoader={setFavLoader}
                                          user={ctx.user ? jwt(api.getCurrentUser().token).username : ''}
                                        />
                                      )
                                    }
                                  } else {
                                    return (
                                      <FavGame
                                        game={y}
                                        reload={() => {
                                          getGames(true)
                                        }}
                                        favLoader={setFavLoader}
                                        user={ctx.user ? jwt(api.getCurrentUser().token).username : ''}
                                      />
                                    )
                                  }
                                }
                              })()}
                              {ctx.preferredLocale === 'th' ? y.nameth : y.name} {'  '}
                            </Label>
                          </Card>
                        </Grid.Column>
                      )
                    })}
                  </Grid>
                )}
              </div>
            </Container>
          </React.Fragment>
        )
      })()}
      {/*END GamesList Area */}
      {/* favLoader Dimmer */}
      <Dimmer.Dimmable blurring dimmed={homeState.favLoader}>
        <Dimmer active={homeState.favLoader} inverted page>
          <Loader active={homeState.favLoader} inline='centered' size='massive'>
            <Translate string={`favLoading${homeState.favMode}`} />
            <br />
            <br />

            <Button
              icon='refresh'
              size='big'
              color='red'
              content='Refresh'
              labelPosition='left'
              onClick={e => {
                e.preventDefault()
                window.location.reload()
              }}
            />
          </Loader>
        </Dimmer>
      </Dimmer.Dimmable>
      {/* End favLoader Dimmer */}
    </div>
  )
}

export default HomeScreen
