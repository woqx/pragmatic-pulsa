import React, { useContext, useState } from 'react'
import Slider from 'react-slick'
import { Container } from 'semantic-ui-react'
import App from '../../App'
import '../../assets/SpecialGames.css'
import { UserContext } from '../../contexts/User'
import Translate, { translate } from '../../locales/Translate'

const SpecialGames = () => {
  const [specialGames, setSpecialGames] = useState(window.specialGames[process.env.REACT_APP_MAIN_PARENT])
  const ctx = useContext(UserContext)
  const isAppMobile = /Mobi/.test(navigator.userAgent)
  const slidesToShow = isAppMobile ? 1 : 2
  const bannerSettings = {
    infinite: true,

    swipeToSlide: true,
    autoplaySpeed: 2000,
    speed: 500,
    dots: false,
    arrows: true,

    adaptiveHeight: true,
    slidesToShow: 2,
    // autoplay: isAppMobile ? true : false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 768,
        settings: {
          autoplay: true,
          slidesToScroll: 1,
          slidesToShow: 1,
          initialSlide: 0,
          fade: true,
        },
      },
    ],
  }
  return (
    <Container className='containerSpecialGames'>
      <Slider {...bannerSettings} className='bannerStyle'>
        {specialGames.map(sg => (
          <div className='div-1'>
            <img className='gamesBanner' src={sg.image} />
            <div className='btnBox'>
              <a
                href='#'
                onClick={e => {
                  e.preventDefault()
                  App.gameLoader(ctx)
                  if (!ctx.user) {
                    App.gameLoader(ctx)
                    App.showErr(ctx, 'err.PleaseLogin', '/login')
                    return
                  }
                  window.specialGamesLaunch(sg.provider, ctx, false, App, process.env.REACT_APP_BACKEND_URL, sg.mType)
                }}>
                <div className='action_call'>{sg.locales[ctx.preferredLocale]}</div>
              </a>
            </div>
          </div>
        ))}
      </Slider>
    </Container>
  )
}

export default SpecialGames
