import React from 'react'
import { Header, Icon, Image, Card, Container } from 'semantic-ui-react'

export default class extends React.Component {

  componentDidMount() {
    setTimeout(() => {
      window.location.href = '/';
    }, 4000)
  }

  render() {
    return (
      <Container>
        <Card className="custom-content">
          <Header as='h2' icon textAlign='center'>
            <Icon name='users' circular />
            <Header.Content>Thank you!</Header.Content>
            <Header.Subheader>Your deposit process has been completed! You will be redirect to the lobby! Enjoy our game and play responsibly!</Header.Subheader>
          </Header>
        </Card>
      </Container>
    )
  }
}