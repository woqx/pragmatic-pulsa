import React from 'react'
import api from '../api'

export const UserContext = React.createContext({
  user: api.getCurrentUser(),
  updateUser: api.setCurrentUser,
  logout: () => {},
})
