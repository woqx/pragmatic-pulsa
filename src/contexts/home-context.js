import React from 'react'
import api from '../api'

export const HomeContext = React.createContext({
  gameLoader: false,
  setGameLoader: () => {},
  sitesInfo: false,
})
