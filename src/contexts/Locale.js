import React from 'react'

export const LocaleContext = React.createContext({
  preferredLocale: 'id',
  changeLanguage: () => {},
})
